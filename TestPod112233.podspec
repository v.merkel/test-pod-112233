Pod::Spec.new do |s|
  s.name             = 'TestPod112233'
  s.version          = '1.0.1'
  s.summary          = 'Test pod 112233.'
  s.homepage         = 'https://gitlab.com/v.merkel/test-pod-112233'
  s.author           = 'v.merkel'
  s.source           = { :http => 'http://localhost:7788/TestPod112233.framework.zip' }
  s.ios.deployment_target = '10.0'
  s.vendored_frameworks = 'TestPod112233.framework'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
